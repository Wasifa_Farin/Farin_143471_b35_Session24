<?php

namespace App\Model;
use PDO;
use PDOException;

class Database
{
    public $DBH;
    public $host="localhost";
    public $dbname="atomic_project_b35";
    public $username="root";
    public $password="";

    public function __construct()
    {
        try
        {
            $this->DBH= new PDO("mysql:host=$this->host;dbname=$this->dbname",$this->username,$this->password);
            //echo "Connected Successfully!";
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}

