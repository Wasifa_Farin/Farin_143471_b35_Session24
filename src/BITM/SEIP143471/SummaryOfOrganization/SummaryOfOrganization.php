<?php
namespace App\SummaryOfOrganization;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class SummaryOfOrganization extends DB
{
    public $id="";
    public $organization_name="";
    public $summary_of_organization="";

    public function __construct(){

        parent::__construct();
    }

    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('organization_name',$data)){
            $this->organization_name=$data['organization_name'];
        }
        if(array_key_exists('summary_of_organization',$data)){
            $this->summary_of_organization=$data['summary_of_organization'];
        }
    }
    public function store(){
        $arrData = array($this->organization_name,$this->summary_of_organization);
        $sql="Insert into summary_of_organization(organization_name, summary_of_organization) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql);
        $result= $STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Failed! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }
}